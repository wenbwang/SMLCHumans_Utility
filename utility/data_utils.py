
from utility.gene_utils import *
from aitviewer.viewer import Viewer as AITViewer
from aitviewer.renderables.meshes import VariableTopologyMeshes as VTMeshes
from aitviewer.renderables.point_clouds import PointClouds as VTPoints


class SMLCHumans_Utils:
    """
    Dataset Utility for SMLCHumans: data loader and viewer
    """

    def __init__(self, dataset_dir=''):
        # init dataset_dir
        self.dataset_dir = dataset_dir
        # init hyper_params
        self.hyper_params = dict()
        # init subj_cloth_seq_dirs
        self.subj_cloth_seq_dirs = dict()


    # # ==================== Data Utilities ==================== # #

    # preprocess mesh{'vertices_origin', 'vertices', 'faces', 'edges', 'colors', 'normals'}: scale, centralize, rotation, offset
    def preprocess_mesh(self, mesh, mcentral=False, bbox=True, rotation=None, offset=None, scale=1.0):
        # get scan vertices mass center
        mcenter = np.mean(mesh['vertices'], axis=0)
        # get scan vertices bbox center
        bmax = np.max(mesh['vertices'], axis=0)
        bmin = np.min(mesh['vertices'], axis=0)
        bcenter = (bmax + bmin) / 2
        # centralize scan data around mass center
        if mcentral:
            mesh['vertices'] -= mcenter
        # centralize scan data around bbox center
        elif bbox:
            mesh['vertices'] -= bcenter

        # scale scan vertices
        mesh['vertices'] /= scale

        # rotate scan vertices
        if rotation is not None:
            mesh['vertices'] = np.matmul(rotation, mesh['vertices'].T).T
        # offset scan vertices
        if offset is not None:
            mesh['vertices'] += offset
        # return scan data, centers, scale
        return mesh, {'mcenter': mcenter, 'bcenter': bcenter}, scale

    # load scan mesh and texture from pkl / obj
    # scan_mesh{vertices, faces, edges, colors, normals ,uvs, uv_image, uv_path}
    def load_scan_mesh(self, mesh_fn, mesh_format='pkl'):
        # load scan mesh from pkl for XHumans
        if mesh_format == 'pkl':
            # locate atlas_fn
            atlas_fn = mesh_fn.replace('mesh-', 'atlas-')
            # load scan mesh and atlas data
            mesh_data, atlas_data = load_pickle(mesh_fn), load_pickle(atlas_fn)
            # load scan uv_coordinate and uv_image as TextureVisuals
            uv_image = Image.fromarray(atlas_data).transpose(method=Image.Transpose.FLIP_TOP_BOTTOM).convert("RGB")
            texture_visual = trimesh.visual.texture.TextureVisuals(uv=mesh_data['uvs'], image=uv_image)
            # pack scan data as trimesh
            scan_trimesh = trimesh.Trimesh(
                vertices=mesh_data['vertices'],
                faces=mesh_data['faces'],
                vertex_normals=mesh_data['normals'],
                visual=texture_visual,
                process=False,
            )
            # pack scan data as mesh
            scan_mesh = {
                'vertices_origin': scan_trimesh.vertices.copy(),
                'vertices': scan_trimesh.vertices.copy(),
                'faces': scan_trimesh.faces,
                'edges': scan_trimesh.edges,
                'colors': scan_trimesh.visual.to_color().vertex_colors,
                # 'normals': scan_trimesh.vertex_normals,
                'uvs': mesh_data['uvs'],
                'uv_image': np.array(uv_image),
                'uv_path': atlas_fn,
            }
            # preprocess scan mesh: scale, centralize, normalize, rotation, offset
            scan_mesh, center, scale = self.preprocess_mesh(scan_mesh, mcentral=False, bbox=True, scale=1.0)
            return scan_mesh, center, scale

        # load scan mesh from obj
        elif mesh_format == 'obj':
            # load scan mesh and atlas data
            mesh_data = trimesh.load(mesh_fn, process=False)
            # load scan uv_image
            atlas_fn = mesh_fn.replace('mesh-', 'atlas-').replace('.{}'.format(mesh_format), '.png')
            uv_image = Image.open(atlas_fn)
            texture_visual = trimesh.visual.texture.TextureVisuals(uv=mesh_data.visual.uv, image=uv_image)
            # pack scan data as trimesh
            scan_trimesh = trimesh.Trimesh(
                vertices=mesh_data.vertices,
                faces=mesh_data.faces,
                vertex_normals=mesh_data.vertex_normals,
                visual=texture_visual,
                process=False,
            )
            # pack scan data as mesh
            scan_mesh = {
                'vertices_origin': scan_trimesh.vertices.copy(),
                'vertices': scan_trimesh.vertices.copy(),
                'faces': scan_trimesh.faces,
                'edges': scan_trimesh.edges,
                'colors': scan_trimesh.visual.to_color().vertex_colors,
                # 'normals': scan_trimesh.vertex_normals,
                'uvs': scan_trimesh.visual.uv,
                'uv_image': np.array(uv_image),
                'uv_path': atlas_fn,
            }
            # preprocess scan mesh: scale, centralize, normalize, rotation, offset
            scan_mesh, center, scale = self.preprocess_mesh(scan_mesh, mcentral=False, bbox=True, scale=1000.0)
            return scan_mesh, center, scale

    # load scan meshes only with preprocess
    def load_scan_meshes(self, scan_mesh_fn, rotation=None, offset=None):
        # load scan data as mesh {'vertices', 'faces', 'edges', 'colors', 'normals', 'uvs', 'texture_path'}
        scan_mesh, center, scale = self.load_scan_mesh(scan_mesh_fn)
        # rotate scan and smpl vertices
        if rotation is not None:
            scan_mesh['vertices'] = np.matmul(rotation, scan_mesh['vertices'].T).T
        # offset scan and smpl vertices
        if offset is not None:
            scan_mesh['vertices'] += offset
        return scan_mesh

    # load smpl mesh data from ply / pkl, smpl_mesh{vertices, faces, edges}
    def load_smpl_mesh(self, mesh_fn, gender):
        # load smpl_mesh from ply
        smpl_trimesh = trimesh.load(mesh_fn.replace('.pkl', '.ply'))
        smpl_mesh = {'vertices_origin': smpl_trimesh.vertices.copy(),
                     'vertices': smpl_trimesh.vertices.copy(),
                     'faces': smpl_trimesh.faces,
                     'edges': smpl_trimesh.edges}
        # # load smpl_data from pkl
        # smpl_data = pickle.load(open(mesh_fn, 'rb'))
        # print([[k, v.shape] for k, v in smpl_data.items()])
        return smpl_mesh, smpl_mesh

    # load scan and smpl meshes, centralize with bbox
    def load_scan_smpl_meshes(self, scan_mesh_fn, smpl_mesh_fn, gender, rotation=None, offset=None):
        # load scan data as mesh {'vertices', 'faces', 'edges', 'colors', 'normals', 'uvs', 'texture_path'}
        scan_mesh, center, scale = self.load_scan_mesh(scan_mesh_fn)
        # load smpl data as mesh {'vertices', 'faces', 'edges'}
        smpl_mesh, smpl_cano_mesh = self.load_smpl_mesh(smpl_mesh_fn, gender)
        smpl_mesh['vertices'] -= center['bcenter'] / scale
        # rotate scan and smpl vertices
        if rotation is not None:
            scan_mesh['vertices'] = np.matmul(rotation, scan_mesh['vertices'].T).T
            smpl_mesh['vertices'] = np.matmul(rotation, smpl_mesh['vertices'].T).T
        # offset scan and smpl vertices
        if offset is not None:
            scan_mesh['vertices'] += offset
            smpl_mesh['vertices'] += offset
        return scan_mesh, smpl_mesh, smpl_cano_mesh

    # load segment params from pkl
    def load_segment_params(self, segm_fn):
        # load from pickle file
        segment_params = load_pickle(segm_fn)
        return segment_params


    # # ==================== Subj_Cloth_Seq Utilities ==================== # #

    # load subj_cloth_seq_dir
    def load_subj_cloth_seq_params_dirs(self, subj='', cloth='', seq=''):
        # update hyper_params
        hyper_params = self.load_hyper_params(subj=subj, cloth=cloth, seq=seq)
        # locate subj_cloth_seq dirs
        subj_cloth_seq_dirs = dict()
        subj_cloth_seq_dirs['subj_dir'] = os.path.join(self.dataset_dir, subj)
        subj_cloth_seq_dirs['subj_cloth_dir'] = os.path.join(self.dataset_dir, subj, cloth)
        subj_cloth_seq_dirs['subj_cloth_seq_dir'] = os.path.join(self.dataset_dir, subj, cloth, seq)
        # locate scan, smpl, segm, cloth dirs
        subj_cloth_seq_dirs['scan_dir'] = os.path.join(subj_cloth_seq_dirs['subj_cloth_seq_dir'], 'Meshes_{}'.format(hyper_params['mesh_format']))
        subj_cloth_seq_dirs['smpl_dir'] = os.path.join(subj_cloth_seq_dirs['subj_cloth_seq_dir'], 'SMPL')
        subj_cloth_seq_dirs['smplx_dir'] = os.path.join(subj_cloth_seq_dirs['subj_cloth_seq_dir'], 'SMPLX')
        subj_cloth_seq_dirs['segm_dir'] = os.path.join(subj_cloth_seq_dirs['subj_cloth_seq_dir'], 'Semantic')
        subj_cloth_seq_dirs['capture_dir'] = os.path.join(subj_cloth_seq_dirs['subj_cloth_seq_dir'], 'Capture')

        # locate segment renders, parsers, masks, others
        subj_cloth_seq_dirs['render_dir'] = os.path.join(subj_cloth_seq_dirs['segm_dir'], 'renders')
        subj_cloth_seq_dirs['parser_dir'] = os.path.join(subj_cloth_seq_dirs['segm_dir'], 'parsers')
        subj_cloth_seq_dirs['mask_dir'] = os.path.join(subj_cloth_seq_dirs['segm_dir'], 'masks')
        subj_cloth_seq_dirs['other_dir'] = os.path.join(subj_cloth_seq_dirs['segm_dir'], 'others')

        # locate capture masks, images, cameras, semantics dirs
        subj_cloth_seq_dirs['masks_dir'] = os.path.join(subj_cloth_seq_dirs['capture_dir'], 'masks')
        subj_cloth_seq_dirs['images_dir'] = os.path.join(subj_cloth_seq_dirs['capture_dir'], 'images')
        subj_cloth_seq_dirs['cameras_dir'] = os.path.join(subj_cloth_seq_dirs['capture_dir'], 'cameras')
        subj_cloth_seq_dirs['semantics_dir'] = os.path.join(subj_cloth_seq_dirs['capture_dir'], 'semantics')
        self.subj_cloth_seq_dirs = subj_cloth_seq_dirs
        return hyper_params, subj_cloth_seq_dirs


    # load hyper_params for subj_cloth_seq
    def load_hyper_params(self, subj='', cloth='', seq=''):
        # init hyper_params: rotation, offset for visualization
        hyper_params = {'reality': True, 'gender': 'male', 'mesh_format': 'pkl', 'scan_frames': [],  # subj data
                        'front_back': [0, 6], 'rotation': None, 'offset': None,  # visualization
                        'surface': 'inner', 'surface_labels': ['skin', 'upper', 'lower', 'hair', 'glove', 'shoe'],  # parsing
                        }

        # # ------------------- Set Subj Params ------------------- # #

        # set subj params
        if subj in ['00122']:
            hyper_params['gender'] = 'male'
            hyper_params['front_back'] = [3, 9]
            hyper_params['rotation'] = rotation_matrix(-90, axis='y')

        # set subj params
        if subj in ['00123']:
            hyper_params['gender'] = 'female'
            hyper_params['front_back'] = [2, 8]
            hyper_params['rotation'] = rotation_matrix(-60, axis='y')

        # set subj params
        if subj in ['00127']:
            hyper_params['gender'] = 'male'
            hyper_params['front_back'] = [11, 5]
            hyper_params['rotation'] = rotation_matrix(30, axis='y')

        # set subj params
        if subj in ['00129']:
            hyper_params['gender'] = 'female'
            hyper_params['front_back'] = [4, 10]
            hyper_params['rotation'] = rotation_matrix(-120, axis='y')

        # set subj params
        if subj in ['00132']:
            hyper_params['gender'] = 'female'
            hyper_params['front_back'] = [3, 9]
            hyper_params['rotation'] = rotation_matrix(-90, axis='y')

        # set subj params
        if subj in ['00134']:
            hyper_params['gender'] = 'male'
            hyper_params['front_back'] = [11, 5]
            hyper_params['rotation'] = rotation_matrix(30, axis='y')

        # set subj params
        if subj in ['00135']:
            hyper_params['gender'] = 'male'
            hyper_params['front_back'] = [2, 8]
            hyper_params['rotation'] = rotation_matrix(-60, axis='y')

        # set subj params
        if subj in ['00137']:
            hyper_params['gender'] = 'female'
            hyper_params['front_back'] = [2, 8]
            hyper_params['rotation'] = rotation_matrix(-60, axis='y')

        # set subj params
        if subj in ['00139']:
            hyper_params['gender'] = 'male'
            hyper_params['front_back'] = [11, 5]
            hyper_params['rotation'] = rotation_matrix(30, axis='y')


        # # ------------------- Set Label Params ------------------- # #

        # set surface and surface_labels
        if cloth in ['Outer']:
            hyper_params['surface'] = 'outer'
            hyper_params['surface_labels'] = ['skin', 'upper', 'lower', 'hair', 'glove', 'shoe', 'outer']


        # # ------------------- Load Frame Names ------------------- # #
        # locate scan_meshes from scan_dir
        scan_files = sorted(glob.glob(os.path.join(self.dataset_dir, subj, cloth, seq, 'Meshes_{}'.format(hyper_params['mesh_format']), 'mesh-f*.{}'.format(hyper_params['mesh_format']))))
        scan_frames = [scan_files[ns].split('/')[-1].split('.')[0][-5:] for ns in range(len(scan_files))]
        hyper_params['scan_frames'] = scan_frames
        print('# # ============ Subj_Cloth_Seq: {}_{}_{} // Frames: {}'.format(subj, cloth, seq, len(scan_frames)))

        # # ------------------- Update Hyper Params ------------------- # #
        # update surface_labels
        self.surface = hyper_params['surface']
        self.surface_labels = hyper_params['surface_labels']
        # update hyper_params
        self.hyper_params = hyper_params
        return hyper_params


    # # ==================== AITViewer Utilities ==================== # #

    # view subj_cloth_seq_segment within AITViewers
    def aitview_subj_cloth_seq_segment(self, subj='', cloth='', seq_list='', mode_list='', n_start=0, n_sample=1, n_stop=-1, dist=1.,
                                       view_smpl=False, view_smplx=False, view_vertex_color=False):
        # init AIT Viewers
        aitvs = AITViewer(title='{}_{}_{}'.format(subj, cloth, seq_list))
        aitvs.scene.floor.enabled = False
        aitvs.scene.origin.enabled = False
        aitvs.playback_fps = 5
        # loop over all modes
        for nm in range(len(mode_list)):
            # init segment_meshes
            scan_segment_meshes_modes = []
            smpl_segment_meshes_modes = []
            smplx_segment_meshes_modes = []
            # loop over all sequences
            for seq in seq_list:
                # process subj_cloth_seq_segment into segment_meshes_frames
                scan_segment_frames, smpl_segment_frames, smplx_segment_frames = self.process_subj_cloth_seq_segment(
                    subj=subj, cloth=cloth, seq=seq, mode=mode_list[nm], n_start=n_start, n_sample=n_sample, n_stop=n_stop,
                    process_smpl=view_smpl, process_smplx=view_smplx)
                # append scan and smpl frames
                if scan_segment_frames is not None:
                    scan_segment_meshes_modes += scan_segment_frames
                if smpl_segment_frames is not None:
                    smpl_segment_meshes_modes += smpl_segment_frames
                if smplx_segment_frames is not None:
                    smplx_segment_meshes_modes += smplx_segment_frames

            # view segment_meshes_frames within AIT Viewers
            self.aitview_segment_meshes(scan_segment_meshes_modes, smpl_segment_meshes_modes, smplx_segment_meshes_modes, aitvs,
                                        self.surface_labels, base=np.array([0., 0. - nm * 4., 0.]), dist=dist,
                                        view_subsurface=True, view_vertex_color=view_vertex_color)
        # run AIT Viewers
        aitvs.run()

    # view segment_meshes within AITViewer
    def aitview_segment_meshes(self, scan_segment_frames, smpl_segment_frames, smplx_segment_frames, aitvs, surface_labels,
                               base=np.array([0., 0., 0.]), dist=1., view_subsurface=False, view_vertex_color=False):
        # view_smpl
        view_smpl = True if smpl_segment_frames is not None and len(smpl_segment_frames) > 0 and smpl_segment_frames[0] is not None else False
        view_smplx = True if smplx_segment_frames is not None and len(smplx_segment_frames) > 0 and smplx_segment_frames[0] is not None else False
        # init segment_meshes{'mesh': {'vertices': [], 'faces': [], 'colors': []}}
        segment_meshes, frame_nums = dict(), len(scan_segment_frames)
        # append all scan segment meshes
        for nf in range(len(scan_segment_frames)):
            # init scan_segment_meshes
            scan_segment_meshes = scan_segment_frames[nf]
            for key, value in scan_segment_meshes.items():
                # init scan segment meshes
                if nf == 0: segment_meshes['scan_' + key] = {'vertices': [], 'faces': [], 'colors': [], 'uvs': [], 'uv_image': [], 'uv_path': []}
                # skip empty key
                if not 'scan_' + key in segment_meshes: continue
                # append segment meshes
                for k, v in segment_meshes['scan_' + key].items():
                    if k in scan_segment_meshes[key]:
                        segment_meshes['scan_' + key][k].append(scan_segment_meshes[key][k])
                        # segment_meshes['scan_' + key][k][nf] = scan_segment_meshes[key][k]
            # append all smpl segment meshes
            if view_smpl and smpl_segment_frames[nf] is not None:
                # init scan_segment_meshes
                smpl_segment_meshes = smpl_segment_frames[nf]
                for key, value in smpl_segment_frames[0].items():
                    # init smpl_segment meshes
                    if nf == 0: segment_meshes['smpl_' + key] = {'vertices': [], 'faces': [], 'colors': []}
                    # append segment meshes
                    for k, v in smpl_segment_meshes[key].items():
                        if k in smpl_segment_meshes[key]:
                            segment_meshes['smpl_' + key][k].append(smpl_segment_meshes[key][k])

            # append all smplx segment meshes
            if view_smplx and smplx_segment_frames[nf] is not None:
                # init scan_segment_meshes
                smplx_segment_meshes = smplx_segment_frames[nf]
                for key, value in smplx_segment_frames[0].items():
                    # init smpl_segment meshes
                    if nf == 0: segment_meshes['smplx_' + key] = {'vertices': [], 'faces': [], 'colors': []}
                    # append segment meshes
                    for k, v in smplx_segment_meshes[key].items():
                        if k in smplx_segment_meshes[key]:
                            segment_meshes['smplx_' + key][k].append(smplx_segment_meshes[key][k])

        # view mesh using vertex color
        if view_vertex_color:
            # AIT view front scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh']['vertices'], segment_meshes['scan_mesh']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh']['colors'],
                                     position=np.array([-1. * dist, 0., 0.]) + base, name='scan_mesh'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh_back']['vertices'], segment_meshes['scan_mesh_back']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh_back']['colors'],
                                     position=np.array([-1. * dist, 2., 0.]) + base, name='scan_mesh_back'))
            aitvs.scene.nodes[-1].backface_culling = False
        # view mesh using texture color
        else:
            # AIT view front scan_mesh, using uvs and uv_path
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh']['vertices'], segment_meshes['scan_mesh']['faces'],
                                     uv_coords=segment_meshes['scan_mesh']['uvs'], texture_paths=segment_meshes['scan_mesh']['uv_path'],
                                     position=np.array([-1. * dist, 0., 0.]) + base, name='scan_mesh'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back scan_mesh, using uvs and uv_path
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh_back']['vertices'], segment_meshes['scan_mesh_back']['faces'],
                                     uv_coords=segment_meshes['scan_mesh_back']['uvs'], texture_paths=segment_meshes['scan_mesh_back']['uv_path'],
                                     position=np.array([-1. * dist, 2., 0.]) + base, name='scan_mesh_back'))
            aitvs.scene.nodes[-1].backface_culling = False
        # AIT view front scan_label
        aitvs.scene.add(VTMeshes(segment_meshes['scan_label']['vertices'], segment_meshes['scan_label']['faces'],
                                 vertex_colors=segment_meshes['scan_label']['colors'],
                                 position=np.array([0., 0., 0.]) + base, name='scan_label'))
        aitvs.scene.nodes[-1].backface_culling = False
        # AIT view back scan_label
        aitvs.scene.add(VTMeshes(segment_meshes['scan_label_back']['vertices'], segment_meshes['scan_label_back']['faces'],
                                 vertex_colors=segment_meshes['scan_label_back']['colors'],
                                 position=np.array([0., 2., 0.]) + base, name='scan_label_back'))
        aitvs.scene.nodes[-1].backface_culling = False

        # AIT view smpl_label and smpl_mesh
        if view_smpl:
            # AIT view front smpl_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['smpl_label']['vertices'], segment_meshes['smpl_label']['faces'],
                                     vertex_colors=segment_meshes['smpl_label']['colors'],
                                     position=np.array([-3 * dist, 0., 0.]) + base, name='smpl_label'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view front scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh']['vertices'], segment_meshes['scan_mesh']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh']['colors'],
                                     position=np.array([-3. * dist, 0., 0.]) + base, name='scan_mesh'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back smpl_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['smpl_label_back']['vertices'], segment_meshes['smpl_label_back']['faces'],
                                     vertex_colors=segment_meshes['smpl_label']['colors'],
                                     position=np.array([-3 * dist, 2., 0.]) + base, name='smpl_label_back'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh_back']['vertices'], segment_meshes['scan_mesh_back']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh_back']['colors'],
                                     position=np.array([-3. * dist, 2., 0.]) + base, name='scan_mesh_back'))
            aitvs.scene.nodes[-1].backface_culling = False

        # AIT view smpl_label and smpl_mesh
        if view_smplx:
            # AIT view front smpl_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['smplx_label']['vertices'], segment_meshes['smplx_label']['faces'],
                                     vertex_colors=segment_meshes['smplx_label']['colors'],
                                     position=np.array([-4 * dist, 0., 0.]) + base, name='smplx_label'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view front scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh']['vertices'], segment_meshes['scan_mesh']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh']['colors'],
                                     position=np.array([-4. * dist, 0., 0.]) + base, name='scan_mesh'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back smpl_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['smplx_label_back']['vertices'], segment_meshes['smplx_label_back']['faces'],
                                     vertex_colors=segment_meshes['smplx_label']['colors'],
                                     position=np.array([-4 * dist, 2., 0.]) + base, name='smplx_label_back'))
            aitvs.scene.nodes[-1].backface_culling = False
            # AIT view back scan_mesh, using vertex color
            aitvs.scene.add(VTMeshes(segment_meshes['scan_mesh_back']['vertices'], segment_meshes['scan_mesh_back']['faces'],
                                     vertex_colors=segment_meshes['scan_mesh_back']['colors'],
                                     position=np.array([-4. * dist, 2., 0.]) + base, name='scan_mesh_back'))
            aitvs.scene.nodes[-1].backface_culling = False

        # AIT view scan_region, scan_skin, scan_upper, scan_lower, ...
        if view_subsurface:
            # AIT view scan_region
            if 'scan_region' in segment_meshes:
                aitvs.scene.add(VTMeshes(segment_meshes['scan_region']['vertices'], segment_meshes['scan_region']['faces'],
                                         vertex_colors=segment_meshes['scan_region']['colors'],
                                         position=np.array([-3. * dist, 2., 0.]) + base, name='scan_region'))
                aitvs.scene.nodes[-1].backface_culling = False

            # AIT view scan_upper, scan_lower, ...
            for label in surface_labels:
                if 'scan_' + label in segment_meshes:
                    if label in ['skin']:
                        position = np.array([1 * dist, 0., 0.]) + base
                    elif label in ['upper', 'shoe']:
                        position = np.array([2 * dist, 0., 0.]) + base
                    elif label in ['lower', 'hair']:
                        position = np.array([3 * dist, 0., 0.]) + base
                    elif label in ['outer']:
                        position = np.array([-2 * dist, 0., 0.]) + base
                    else:
                        continue

                    if view_vertex_color:
                        # view scan label meshes, using vertex_colors
                        aitvs.scene.add(VTMeshes(segment_meshes['scan_' + label]['vertices'], segment_meshes['scan_' + label]['faces'],
                                                 vertex_colors=segment_meshes['scan_' + label]['colors'],
                                                 position=position, name='scan_' + label))
                        aitvs.scene.nodes[-1].backface_culling = False
                        # view scan label points, using vertex_colors
                        aitvs.scene.add(VTPoints(segment_meshes['scan_' + label]['vertices'], segment_meshes['scan_' + label]['colors'],
                                                 position=position + np.array([0., 2., 0.]), name='point_' + label))
                    else:
                        # view scan label meshes, using uvs and uv_path
                        aitvs.scene.add(VTMeshes(segment_meshes['scan_' + label]['vertices'], segment_meshes['scan_' + label]['faces'],
                                                 uv_coords=segment_meshes['scan_' + label]['uvs'], texture_paths=segment_meshes['scan_' + label]['uv_path'],
                                                 position=position, name='scan_' + label))
                        aitvs.scene.nodes[-1].backface_culling = False
                        # view scan label points, using vertex_colors
                        aitvs.scene.add(VTPoints(segment_meshes['scan_' + label]['vertices'], segment_meshes['scan_' + label]['colors'],
                                                 position=position + np.array([0., 2., 0.]), name='point_' + label))


    # # ==================== Extract Segment Meshes Utilities ==================== # #

    # process subj_cloth_seq_segment data, with smpl or smplx
    def process_subj_cloth_seq_segment(self, subj='', cloth='', seq='', mode='', n_start=0, n_sample=1, n_stop=-1,
                                       process_smpl=False, process_smplx=False):
        # update hyper_params
        hyper_params, subj_cloth_seq_dirs = self.load_subj_cloth_seq_params_dirs(subj=subj, cloth=cloth, seq=seq)
        # locate scan_dir, smpl_dir, segm_dir, cloth_dir
        segm_dir = os.path.join(subj_cloth_seq_dirs['segm_dir'], mode)
        # locate scan frames from *.pkl
        scan_files = sorted(glob.glob(os.path.join(subj_cloth_seq_dirs['scan_dir'], 'mesh-f*')))
        scan_frames = [scan_files[ns].split('/')[-1].split('.')[0][-5:] for ns in range(len(scan_files))]
        # init first_frame and last_frame
        first_frame, last_frame = scan_frames[0], scan_frames[-1]
        print('first_frame, last_frame:', first_frame, last_frame)
        # init segment_meshes_frames: [{'scan_mesh', 'scan_label', 'scan_personal', 'skin', 'upper', 'lower', 'hair', 'glove', 'shoe'}, ...]
        scan_segment_frames = []
        smpl_segment_frames = [] if process_smpl else None
        smplx_segment_frames = [] if process_smplx else None

        # loop over all sampled scan frames
        loop = tqdm(range(n_start // n_sample, len(scan_files) // n_sample))
        for n_frame in loop:
            # check stop frame
            if 0 <= n_stop < n_frame * n_sample: break
            # locate current frame
            frame = scan_frames[n_frame * n_sample]
            loop.set_description('## Loading Frame for {}_{}_{}: {}/{}'.format(subj, cloth, seq, frame, last_frame))

            # locate scan and smpl data
            scan_mesh_fn = os.path.join(subj_cloth_seq_dirs['scan_dir'], 'mesh-f{}.{}'.format(frame, hyper_params['mesh_format']))
            smpl_mesh_fn = os.path.join(subj_cloth_seq_dirs['smpl_dir'], 'mesh-f{}_smpl.{}'.format(frame, hyper_params['mesh_format']))
            smplx_mesh_fn = os.path.join(subj_cloth_seq_dirs['smplx_dir'], 'mesh-f{}_smplx.{}'.format(frame, hyper_params['mesh_format']))
            # load segment_params
            opt_params = dict()
            if os.path.exists(os.path.join(segm_dir, 'segment-f{}.pkl'.format(frame))):
                opt_params = self.load_segment_params(os.path.join(segm_dir, 'segment-f{}.pkl'.format(frame)))

            # init scan_mesh
            scan_mesh = None
            # process scan and smpl segment meshes
            if process_smpl and os.path.exists(smpl_mesh_fn):
                # load scan and smpl meshes
                scan_mesh, smpl_mesh, smpl_cano_mesh = self.load_scan_smpl_meshes(scan_mesh_fn, smpl_mesh_fn, hyper_params['gender'])
                # init default smpl_labels
                smpl_labels = {'labels': np.zeros(smpl_mesh['vertices'].shape[0])}
                # append process segment meshes
                smpl_segment_frames.append(
                    self.extract_segment_meshes(smpl_mesh, smpl_labels, self.surface_labels,
                                                rotation=hyper_params['rotation'], offset=hyper_params['offset'],
                                                view_boundary=True, view_canonical=False, view_subsurface=False, view_subsurface_boundary=False))
            # process scan and smplx segment meshes
            if process_smplx and os.path.exists(smplx_mesh_fn):
                # load scan and smplx meshes
                scan_mesh, smplx_mesh, smplx_cano_mesh = self.load_scan_smpl_meshes(scan_mesh_fn, smplx_mesh_fn, hyper_params['gender'])
                # init default smpl_labels
                smplx_labels = {'labels': np.zeros(smplx_mesh['vertices'].shape[0])}
                # append process segment meshes
                smplx_segment_frames.append(
                    self.extract_segment_meshes(smplx_mesh, smplx_labels, self.surface_labels,
                                                rotation=hyper_params['rotation'], offset=hyper_params['offset'],
                                                view_boundary=True, view_canonical=False, view_subsurface=False, view_subsurface_boundary=False))
            # load scan_mesh only
            if scan_mesh is None: scan_mesh = self.load_scan_meshes(scan_mesh_fn)

            # init default scan_labels
            if 'scan_params' not in opt_params:
                opt_params['scan_params'] = {'labels': np.zeros(scan_mesh['vertices'].shape[0])}
            else:
                opt_params['scan_params'] = {'labels': opt_params['scan_params']['labels'].cpu().numpy()}

            # append process segment meshes: {'scan_mesh', 'scan_label', 'scan_personal', 'skin', 'upper', 'lower', 'hair', 'glove', 'shoe'}
            scan_segment_frames.append(
                self.extract_segment_meshes(scan_mesh, opt_params['scan_params'], self.surface_labels,
                                            rotation=hyper_params['rotation'], offset=hyper_params['offset'],
                                            view_boundary=False, view_canonical=False, view_subsurface=True, view_subsurface_boundary=True))
        # return segment_frames[{{}, ...}, ...]
        return scan_segment_frames, smpl_segment_frames, smplx_segment_frames


    # extract segment_meshes: {mesh_front, mesh_back, label_front, label_back, skin, upper, lower, hair, shoe, outer}
    def extract_segment_meshes(self, mesh, params, surface_labels, rotation=None, offset=None,
                               view_boundary=False, view_canonical=False, view_subsurface=False, view_subsurface_boundary=False):
        # init mesh_vertices
        mesh_vertices = mesh['vertices'].copy()
        # get mesh_vertices bbox center
        bcenter = (np.max(mesh['vertices_origin'], axis=0) + np.min(mesh['vertices_origin'], axis=0)) / 2
        # init mesh faces and labels
        mesh_faces, mesh_labels = mesh['faces'], params['labels']
        # rotate mesh vertices
        if rotation is not None and not view_canonical:
            mesh_vertices = np.matmul(rotation, mesh_vertices.T).T
        # offset mesh vertices
        if offset is not None and not view_canonical:
            mesh_vertices += offset

        # init segment_meshes
        segment_meshes = dict()
        # get boundary label and region
        if view_boundary and 'boundarys' in params:
            # init boundary labels and regions
            mesh_boundary_labels = np.zeros(mesh_vertices.shape[0])
            mesh_boundary_regions = np.zeros(mesh_vertices.shape[0])
            # append boundary labels and regions for each boundary
            for key, value in params['boundarys'].items():
                mesh_boundary_labels += params['boundarys'][key]['labels'].cpu().numpy()
                mesh_boundary_regions += np.sum(params['boundarys'][key]['regions'].cpu().numpy(), axis=-1)
            # assign region
            segment_meshes['region'] = {'vertices': mesh_vertices, 'faces': mesh_faces}
            if 'colors' in mesh:
                segment_meshes['region']['colors'] = mask_texture_color(mesh['colors'] / 255., mask=mesh_boundary_regions > 0)
            # render mesh_label_colors with boundary and region
            if view_boundary:
                mesh_label_colors = render_label_colors(mesh_labels, boundary=mesh_boundary_labels > 0, region=mesh_boundary_regions > 0)
            else:
                mesh_label_colors = render_label_colors(mesh_labels, boundary=None, region=None)
        else:
            mesh_label_colors = render_label_colors(mesh_labels, boundary=None, region=None)

        # assign mesh: {'vertices', 'faces', 'colors', 'uvs', 'uv_image', 'uv_path'}
        if view_canonical:
            segment_meshes['mesh'] = {'vertices': np.matmul(rotation_matrix(180, axis='y'), mesh_vertices.T).T,
                                      'faces': mesh_faces, 'colors': mesh_label_colors}
        else:
            segment_meshes['mesh'] = {'vertices': mesh_vertices, 'faces': mesh_faces}
            segment_meshes['mesh_back'] = {'vertices': np.matmul(rotation_matrix(180, axis='y'), mesh_vertices.T).T, 'faces': mesh_faces}

            # assign mesh colors
            if 'colors' in mesh:
                segment_meshes['mesh']['colors'] = mesh['colors'] / 255.
                segment_meshes['mesh_back']['colors'] = mesh['colors'] / 255.
            # assign mesh uvs, uv_image, uv_path
            for key in ['uvs', 'uv_image', 'uv_path']:
                if key in mesh:
                    segment_meshes['mesh'][key] = mesh[key]
                    segment_meshes['mesh_back'][key] = mesh[key]

        # assign label: {'vertices', 'faces', 'colors'}
        segment_meshes['label'] = {'vertices': mesh_vertices, 'faces': mesh_faces, 'colors': mesh_label_colors}
        segment_meshes['label_back'] = {'vertices': np.matmul(rotation_matrix(180, axis='y'), mesh_vertices.T).T, 'faces': mesh_faces, 'colors': mesh_label_colors}

        # return mesh and label
        if not view_subsurface:
            return segment_meshes
        # return mesh, label, region, skin, garments
        else:
            # extract personal mesh
            segment_meshes['personal'] = {'vertices': mesh_vertices, 'faces': mesh_faces}
            if 'colors' in mesh:
                segment_meshes['personal']['colors'] = mask_texture_color(mesh['colors'] / 255., mask=mesh_labels == 0)

            # cloth_tracker: bbox centralize cloth meshes
            if 'cloth_meshes' in params:
                # centralize cloth_meshes with bcenter
                cloth_meshes = copy.deepcopy(params['cloth_meshes'])
                for key, value in cloth_meshes.items():
                    cloth_meshes[key]['vertices'] -= bcenter
                # rotate mesh vertices for visualization
                if rotation is not None and not view_canonical:
                    for key, value in cloth_meshes.items():
                        cloth_meshes[key]['vertices'] = np.matmul(rotation, cloth_meshes[key]['vertices'].T).T
                # offset mesh vertices for visualization
                if offset is not None and not view_canonical:
                    for key, value in cloth_meshes.items():
                        cloth_meshes[key]['vertices'] += offset
            # surface_parser: extract cloth_meshes: {'label': {'vertices', 'faces', 'colors', 'boundarys'}}
            else:
                cloth_meshes = self.extract_label_meshes(mesh_vertices, mesh_faces, mesh_labels, surface_labels,
                                                         colors=mesh['colors'] / 255., uvs=mesh['uvs'] if 'uvs' in mesh else None,
                                                         uv_image=mesh['uv_image'] if 'uv_image' in mesh else None,
                                                         uv_path=mesh['uv_path'] if 'uv_path' in mesh else None)
            # print([[key, value['vertices'].shape[0], value['faces'].shape[0]] for key, value in cloth_meshes.items()])

            # assign cloth_meshes to segment_meshes
            for key, value in cloth_meshes.items():
                # assign segment meshes
                segment_meshes[key] = cloth_meshes[key].copy()
                # assign subsurface boundary colors
                if view_subsurface_boundary and 'cloth_boundarys' in params:
                    if key not in params['cloth_boundarys']: continue
                    segment_meshes[key]['boundarys'] = params['cloth_boundarys'][key]
                    segment_meshes[key]['colors'] = render_boundary_colors(segment_meshes[key]['colors'], params['cloth_boundarys'][key]['regions'])
                    segment_meshes[key]['boundary_mesh'] = extract_boundary_mesh(segment_meshes[key]['vertices'], segment_meshes[key]['faces'], segment_meshes[key]['colors'], segment_meshes[key]['boundarys'])
        return segment_meshes


    # extract label meshes: upper, lower, hair, shoe, outer, ...
    def extract_label_meshes(self, vertices, faces, labels, surface_labels, colors=None, uvs=None, uv_image=None, uv_path=None, target_labels=None):
        # init label_meshes and face_labels
        label_meshes = dict()
        face_labels = labels[faces]
        # loop over all labels
        for nl in range(len(surface_labels)):
            # skip empty label
            if np.sum(labels == nl) == 0: continue
            # skip non-target_labels
            if target_labels is not None and surface_labels[nl] not in target_labels: continue

            # # TODO: find label faces: with label vertices >= 2
            # face_label_nl = np.where(np.sum(face_labels == nl, axis=-1) >= 2)[0]
            # vertex_label_nl = np.unique(faces[face_label_nl].flatten())

            # TODO: find label faces: with label vertices == 3
            vertex_label_nl = np.where(labels == nl)[0]
            face_label_nl = np.where(np.sum(face_labels == nl, axis=-1) == 3)[0]

            # find correct indices
            correct_indices = (np.zeros(labels.shape[0]) - 1).astype(int)
            correct_indices[vertex_label_nl] = np.arange(vertex_label_nl.shape[0])

            # extract label_mesh[vertices, faces]
            label_meshes[surface_labels[nl]] = {'vertices': vertices[vertex_label_nl], 'faces': correct_indices[faces[face_label_nl]]}
            # extract label_mesh colors
            if colors is not None:
                label_meshes[surface_labels[nl]]['colors'] = colors[vertex_label_nl]
            # extract label_mesh uvs
            if uvs is not None:
                label_meshes[surface_labels[nl]]['uvs'] = uvs[vertex_label_nl]
            # keep label_mesh uv_image
            if uv_image is not None:
                label_meshes[surface_labels[nl]]['uv_image'] = uv_image
            # keep label_mesh uv_path
            if uv_path is not None:
                label_meshes[surface_labels[nl]]['uv_path'] = uv_path
        return label_meshes
