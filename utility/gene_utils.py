import os
import glob
import pickle
import trimesh
import numpy as np

from PIL import Image
from tqdm import tqdm

# set label_color: skin0, upper1, lower2, hair3, glove4, shoe5, outer6
SURFACE_LABEL = ['skin', 'upper', 'lower', 'hair', 'glove', 'shoe', 'outer']
SURFACE_LABEL_COLOR = np.array([[0, 0, 0], [255, 0, 0], [0, 255, 0], [255, 128, 0], [128, 128, 0], [128, 0, 255], [0, 128, 255]])


# load data from pkl_dir
def load_pickle(pkl_dir):
    return pickle.load(open(pkl_dir, "rb"))

# get xyz rotation matrix
def rotation_matrix(angle, axis='x'):
    # get cos and sin from angle
    c, s = np.cos(np.radians(angle)), np.sin(np.radians(angle))
    # get totation matrix
    R = np.array([[1, 0, 0], [0, c, -s], [0, s, c]])
    if axis == 'y':
        R = np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])
    if axis == 'z':
        R = np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])
    return R

# render texture color with target label
def mask_texture_color(texture, mask):
    # init white colors
    colors = np.ones(texture.shape)
    # assign texture to color at target label
    colors[mask, :] = texture[mask, :]
    return colors

# render label_colors(h, w, 4) according to label_values: skin 0, upper 1, lower 2,
def render_label_colors(label, boundary=None, region=None):
    # check non-zero label
    if np.sum(label) == 0: return None

    # init black color
    colors = np.zeros((label.shape[0], 3))
    # assign label color
    for nl in range(len(SURFACE_LABEL)):
        colors[label == nl] = SURFACE_LABEL_COLOR[nl]
    # assign region color
    if region is not None:
        colors[region, :] = np.array([255, 255, 0])
    # assign boundary color
    if boundary is not None:
        colors[boundary, :] = np.array([0, 0, 255])
    # append color with the fourth channel
    colors = np.append(colors, np.ones((colors.shape[0], 1)) * 255, axis=-1) / 255.
    return colors

