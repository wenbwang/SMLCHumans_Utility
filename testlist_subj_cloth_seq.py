
# SUBJ_CLOTH_SEQ_TESTLIST for all subjects, clothes, and sequences ['Take*', start_frame, end_frame]
SUBJ_CLOTH_SEQ_TESTLIST = {
    # 20230621_00122_Take*
    '00122': {
        'Inner': [['Take5', 1, 150], ['Take8', 11, 160]],
        'Outer': [['Take11', 1, 100], ['Take16', 16, 265]]
    },
    # 20230621_00123_Take*
    '00123': {
        'Inner': [['Take3', 1, 150], ['Take5', 1, 150]],
        'Outer': [['Take10', 1, 160], ['Take11', 1, 150]]
    },
    # 20230629_00127_Take*
    '00127': {
        'Inner': [['Take8', 11, 140], ['Take9', 11, 150]],
        'Outer': [['Take16', 11, 150], ['Take18', 6, 155]]
    },
    # 20230823_00129_Take*
    '00129': {
        'Inner': [['Take3', 1, 160], ['Take5', 1, 130]],
        'Outer': [['Take11', 1, 160], ['Take13', 1, 140]]
    },
    # 20230823_00132_Take*
    '00132': {
        'Inner': [['Take2', 11, 180], ['Take9', 11, 180]],
        'Outer': [['Take20', 1, 170], ['Take21', 1, 200]]
    },
    # 20230823_00134_Take*
    '00134': {
        'Inner': [['Take5', 16, 165], ['Take6', 6, 110]],
        'Outer': [['Take12', 11, 165], ['Take19', 1, 160]]
    },
    # 20230823_00135_Take*
    '00135': {
        'Inner': [['Take7', 16, 170], ['Take10', 6, 190]],
        'Outer': [['Take21', 6, 150], ['Take24', 6, 130]]
    },
    # 20230824_00137_Take*
    '00137': {
        'Inner': [['Take5', 21, 170], ['Take7', 11, 160]],
        'Outer': [['Take16', 1, 150], ['Take19', 21, 150]]
    },
    # 20230824_00139_Take*
    '00139': {
        'Inner': [['Take6', 21, 170], ['Take9', 11, 160]],
        'Outer': [['Take18', 11, 160], ['Take24', 11, 190]]
    },
    # 20230824_00140_Take*
    '00140': {
        'Inner': [['Take6', 11, 200], ['Take8', 11, 110]],
        'Outer': [['Take19', 11, 170], ['Take21', 1, 120]]
    },
}