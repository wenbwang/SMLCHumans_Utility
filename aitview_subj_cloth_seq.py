import argparse
from utility.data_utils import SMLCHumans_Utils
from testlist_subj_cloth_seq import SUBJ_CLOTH_SEQ_TESTLIST


# ------------------------ Render & Parser Subj_Cloth_Seq Meshes ------------------------ # #

# set subj_cloth_seq target parser
parser = argparse.ArgumentParser()
parser.add_argument('--subj', default='00127', help='subj name')
parser.add_argument('--cloth', default='Outer', help='cloth name')
parser.add_argument('--seq', default=None, help='seq name')
parser.add_argument('--segm', default='semantic_auto', help='segm name')
args = parser.parse_args()
print('## Command subj, cloth, seq, segm:', args.subj, args.cloth, args.seq, args.segm)


# # ------------------------ Locate Dataset Folders ------------------------ # #

# TODO:locate SMLCHumans dataset
# SMLCHumans_dir = "/Somewhere/SMLCHumans"
SMLCHumans_dir = "/run/user/1000/gvfs/smb-share:server=mocap-stor-02.inf.ethz.ch,share=work/Clothes4D/SMLCHumans"

# init SMLCHumans_Utils
dataset_utils = SMLCHumans_Utils(dataset_dir=SMLCHumans_dir)

# load test_subj_cloth_seq
if args.seq is None: args.seq = SUBJ_CLOTH_SEQ_TESTLIST[args.subj][args.cloth][0][0]

# aitview subj_cloth_seq_segment, view_smpl, view_smplx, view_vertex_color (fast) or view_uv_color (slow)
dataset_utils.aitview_subj_cloth_seq_segment(subj=args.subj, cloth=args.cloth, seq_list=[args.seq], mode_list=[args.segm],
                                             n_start=0, n_sample=1, n_stop=-1, dist=1.5, view_smpl=True, view_smplx=True, view_vertex_color=False)
